"use strict";

/* Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript. 
Это метод связи с сервером через НТТР протокол */

const URI = "https://swapi.dev/api/films/";

function getFilms(URI) {
  return fetch(URI).then((response) => response.json());
}

getFilms(URI)
  .then((data) => {
    // sort by number of episode
    data.results
      .sort(({ episode_id }, { episode_id: episode_id2 }) => {
        return episode_id - episode_id2;
      })
      // adding title and crawl
      .map((film) => {
        const p = document.createElement("p");
        const div = document.createElement("div");
        const p2 = document.createElement("p");

        p.style.fontWeight = "bold";
        p.textContent = `${film.episode_id}: ${film.title}`;

        div.id = film.episode_id;

        // adding animathion
        div.innerHTML =
          '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>';

        p2.textContent = film.opening_crawl;

        document.body.append(p);
        document.body.append(div);
        document.body.append(p2);
      });
    return data;
  })
  //adding characters
  .then(({ results }) => {
    results.map(({ characters, episode_id }) => {
      const fragment = document.createElement("div");

      let counter = 0;

      characters.forEach((character) => {
        getFilms(character)
          .then((characterResponse) => {
            counter++;

            const span = document.createElement("span");
            span.textContent = `${characterResponse.name}, `;
            fragment.append(span);

            if (characters.length === counter) {
              const div = document.getElementById(episode_id);
              div.textContent = "Characters: ";
              div.append(fragment);
            }
          })
          .catch((err) => {
            console.log(err);
          });
      });
    });
  })
  .catch((err) => {
    console.log(err);
  });
